import React, { useEffect, useState } from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";
import HeaderSignin from "../Components/Header/HeaderSignin";

export default function LayoutSignin({ children }) {
  return (
    <div className="flex flex-col justify-between min-h-[100vh]">
      <HeaderSignin />
      <div className="bg-sky-100 flex justify-center items-center grow">
        {children}
      </div>
      <Footer />
    </div>
  );
}
