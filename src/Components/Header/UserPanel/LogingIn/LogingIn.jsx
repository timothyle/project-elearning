import React from "react";
import { Dropdown, message } from "antd";
import { UilUserCircle } from "@iconscout/react-unicons";
import { userLocalService } from "../../../../services/localStorageService";
import { NavLink } from "react-router-dom";

export default function LogingIn({ userInfo }) {
  const handleDangXuat = () => {
    userLocalService.remove();
    message.success("Đăng xuất thành công");
    setTimeout(() => {
      window.location.href = "/";
    }, 1000);
  };
  const items = [
    {
      label: <NavLink to="/ThongTinTaiKhoan">Thông tin người dùng</NavLink>,
      key: "0",
    },
    {
      label: <a>2nd menu item</a>,
      disabled: true,
      key: "1",
    },
    {
      type: "divider",
    },
    {
      label: (
        <p className="text-red-500" onClick={handleDangXuat}>
          Đăng xuất
        </p>
      ),
      key: "3",
    },
  ];
  return (
    <div className="translate-y-[1px] min-w-max">
      <Dropdown
        menu={{
          items,
        }}
      >
        <div
          className="text-xl cursor-pointer w-full min-w-[200px] text-white"
          onClick={(e) => e.preventDefault()}
        >
          <NavLink to="/ThongTinTaiKhoan">
            <UilUserCircle
              size={34}
              className="inline-block mr-2 translate-y-[-2px]"
            />
            Xin chào, {userInfo.hoTen}
          </NavLink>
        </div>
      </Dropdown>
    </div>
  );
}
