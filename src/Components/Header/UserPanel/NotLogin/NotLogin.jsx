import { Button } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";

export default function NotLogin() {
  return (
    <div className="flex gap-2 mx-5">
      <NavLink to="/DangNhap" className="h-full">
        <Button
          type="primary"
          className="bg-blue-500 text-white font-semibold duration-150 border border-blue-300"
        >
          Đăng nhập
        </Button>
      </NavLink>
      <NavLink to="/DangKy">
        <Button
          type="success"
          className="bg-emerald-500 text-white font-semibold duration-150 hover:bg-emerald-400 active:bg-emerald-700 border border-emerald-400"
        >
          Đăng ký
        </Button>
      </NavLink>
    </div>
  );
}
