import React from "react";
import Logo from "../Logo/Logo";
import DanhMuc from "./DanhMuc/DanhMuc";
import SearchBar from "./SearchBar/SearchBar";
import UserPanel from "./UserPanel/UserPanel";

export default function Header() {
  return (
    <div className="bg-sky-500 p-3 flex justify-between gap-5 items-center">
      <Logo />
      <DanhMuc />
      <SearchBar />
      <UserPanel />
    </div>
  );
}
