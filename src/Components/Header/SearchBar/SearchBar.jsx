import Search from "antd/es/input/Search";
import React from "react";
import { useNavigate } from "react-router-dom";
import { UilSearch } from "@iconscout/react-unicons";

export default function SearchBar() {
  const navigate = useNavigate();

  const onSearch = (value) => {
    if (value == "") {
      return;
    } else {
      navigate(`/TimKiemKhoaHoc/${value}`);
    }
  };
  return (
    <div className="w-full relative">
      <Search
        placeholder="Tìm kiếm"
        allowClear
        onSearch={onSearch}
        enterButton={<UilSearch />}
        style={{
          border: "1px solid #60d0dc",
          borderRadius: "10px",
          overflow: "hidden",
        }}
      />
    </div>
  );
}
