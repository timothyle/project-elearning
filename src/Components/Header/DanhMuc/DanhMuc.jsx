import { Dropdown } from "antd";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { learningService } from "../../../services/learningService";
import { UisBars } from "@iconscout/react-unicons-solid";
import { store } from "../../../app/store";
import { setDanhMucKhoaHoc } from "../../../redux-toolkit/slice/khoaHocSlicer";

export default function DanhMuc() {
  const [arrDanhMuc, setArrDanhMuc] = useState([]);
  useEffect(() => {
    learningService
      .getDanhMucKhoaHoc()
      .then((res) => {
        const danhMuc = res.data.map((item, index) => {
          return {
            key: nanoid(),
            label: (
              <NavLink to={`/DanhMucKhoaHoc/${item.maDanhMuc}`}>
                {item.tenDanhMuc}
              </NavLink>
            ),
          };
        });
        setArrDanhMuc(danhMuc);
        store.dispatch(setDanhMucKhoaHoc(res.data));
      })
      .catch((err) => {});
  }, []);
  return (
    <div>
      <Dropdown menu={{ items: arrDanhMuc }} className="w-max">
        <button className="rounded bg-sky-200 hover:bg-white px-3 py-1">
          <UisBars className="inline-block mr-1 translate-y-[-1px]" /> Danh sách
          khóa học
        </button>
      </Dropdown>
    </div>
  );
}
