import React from "react";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import loading_animate from "../../assets/loading_animate.json";

export default function Loading() {
  let { isLoading } = useSelector((state) => state.loadingSlicer);
  return isLoading ? (
    <div className="fixed top-0 left-0 z-50 bg-sky-400 h-screen w-screen flex flex-col justify-center items-center">
      <Lottie animationData={loading_animate} loop={true} />
      <h2 className="text-white font-bold text-7xl">LOADING</h2>
    </div>
  ) : null;
}
