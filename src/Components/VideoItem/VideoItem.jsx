import React from "react";
import { UisStar } from "@iconscout/react-unicons-solid";
import { UisStarHalfAlt } from "@iconscout/react-unicons-solid";
import { NavLink } from "react-router-dom";
import DangKyButton from "../DangKyButton/DangKyButton";

export default function VideoItem({ item }) {
  return (
    <div className="border border-gray-200 text-left text-lg hover:shadow-xl transition hover:-translate-y-1 duration-300">
      <NavLink
        to={`/ChiTiet/${item.maKhoaHoc}`}
        className="aspect-video flex flex-col justify-center bg-black overflow-hidden text-white font-bold text-center text-4xl"
      >
        <img
          className="object-center w-full"
          src={item.hinhAnh}
          alt={item.tenKhoaHoc}
        />
      </NavLink>
      <div className="py-3 px-2 border-t border-gray-200 relative truncate">
        <NavLink
          to={`/ChiTiet/${item.maKhoaHoc}`}
          className="font-semibold"
          title={item.maKhoaHoc}
        >
          {item.danhMucKhoaHoc.tenDanhMucKhoaHoc + " - " + item.maKhoaHoc}
        </NavLink>
        <h4 className="text-sm text-gray-500">
          Ngày tạo: <span>{item.ngayTao}</span>
        </h4>
        <h4 className="text-sm text-gray-500">{item.luotXem} lượt xem</h4>
        <span>
          <UisStar size="20" className="text-yellow-400 inline-block" />
          <UisStar size="20" className="text-yellow-400 inline-block" />
          <UisStar size="20" className="text-yellow-400 inline-block" />
          <UisStar size="20" className="text-yellow-400 inline-block" />
          <UisStarHalfAlt size="20" className="text-yellow-400 inline-block" />
        </span>
        <div className="absolute bottom-3 right-2">
          <DangKyButton maKhoaHoc={item.maKhoaHoc} />
        </div>
      </div>
    </div>
  );
}
