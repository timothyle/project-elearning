import React from "react";
import { NavLink } from "react-router-dom";

export default function FooterRight() {
  return (
    <div className="flex flex-col justify-start items-center px-5">
      {/* <iframe
        src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
        width={340}
        height={"100%"}
        style={{ border: "none", overflow: "hidden" }}
        scrolling="no"
        frameBorder={0}
        allowFullScreen="true"
        allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
      /> */}
      <div className="text-justify mt-4">
        <NavLink>Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Khởi động Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Lấy đà Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Bật nhảy Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Bay trên không Anh ngữ giao tiếp</NavLink>
        {" - "}
        <NavLink>Tiếp đất</NavLink>
      </div>
    </div>
  );
}
