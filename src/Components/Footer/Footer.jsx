import React from "react";
import FooterLeft from "./FooterLeft/FooterLeft";
import FooterMid from "./FooterMid/FooterMid";
import FooterRight from "./FooterRight/FooterRight";

export default function Footer() {
  return (
    <div className="bg-sky-600 text-white p-5 grid grid-cols-3">
      <FooterLeft />
      <FooterMid />
      <FooterRight />
    </div>
  );
}
