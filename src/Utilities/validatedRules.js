export const rules = {
  account: [
    {
      required: true,
      message: "Vui lòng nhập tên tài khoản của bạn!",
    },
    {
      pattern: /^\S*$/,
      message: "Tên tài khoản không được chứa khoảng trắng!",
    },
  ],
  name: [{ required: true, message: "Vui lòng không để trống họ tên!" }],
  password: [
    { required: true, message: "Vui lòng nhập mật khẩu của bạn!" },
    ({ getFieldValue }) => ({
      validator(rule, value) {
        const errors = [];

        if (!value || getFieldValue("matKhau").length < 8) {
          errors.push("Mật khẩu phải có ít nhất 8 ký tự!");
        }

        if (!/[A-Z]/.test(value) || !/[a-z]/.test(value)) {
          errors.push(
            "Mật khẩu phải chứa ít nhất một chữ cái viết hoa và một chữ cái viết thường!"
          );
        }

        if (!/\d/.test(value)) {
          errors.push("Mật khẩu phải chứa ít nhất một chữ số!");
        }

        if (errors.length > 0) {
          return Promise.reject(errors);
        }

        return Promise.resolve();
      },
    }),
  ],
  repassword: [
    {
      required: true,
      message: "Vui lòng nhập lại mật khẩu của bạn!",
    },
    ({ getFieldValue }) => ({
      validator(rule, value) {
        if (!value || getFieldValue("matKhau") === value) {
          return Promise.resolve();
        }
        return Promise.reject("Mật khẩu không khớp!");
      },
    }),
  ],
  email: [
    { required: true, message: "Vui lòng nhập email của bạn!" },
    {
      pattern: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
      message: "Vui lòng nhập địa chỉ email hợp lệ!",
    },
  ],
  phone: [
    {
      required: true,
      message: "Vui lòng nhập số điện thoại của bạn!",
    },
    {
      pattern: /^(0[3578]|09)[0-9]{8}$/,
      message: "Vui lòng nhập số điện thoại hợp lệ!",
    },
  ],
};
