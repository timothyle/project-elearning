import { https } from "./configURL";

export const userService = {
  postDangNhap: (userData) => {
    return https.post(`/api/QuanLyNguoiDung/DangNhap`, userData);
  },
  postDangky: (userData) => {
    return https.post(`/api/QuanLyNguoiDung/DangKy`, userData);
  },
  putCapNhatThongTin: (userData) => {
    return https.post(
      `/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      userData
    );
  },
  // Lỗi chưa dùng được
  postLayDanhSachKhoaHocChuaGhiDanh: (taiKhoan) => {
    return https.post(
      `/api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh`,
      taiKhoan
    );
  },
  postLayDanhSachKhoaHocChoXetDuyet: (taiKhoan) => {
    return https.post(
      `/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`,
      taiKhoan
    );
  },
};
