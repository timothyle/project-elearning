import axios from "axios";
import { store } from "../app/store";
import {
  setLoadingOff,
  setLoadingOn,
} from "../redux-toolkit/slice/loadingSlicer";
import { userLocalService } from "./localStorageService";

export const BASE_URL = "https://elearningnew.cybersoft.edu.vn";
export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJOb2RlanMgMjgiLCJIZXRIYW5TdHJpbmciOiIyMC8wNy8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODk4MTEyMDAwMDAiLCJuYmYiOjE2NzI5MzgwMDAsImV4cCI6MTY4OTk1ODgwMH0.pLKlIw5mGTkQqXV5wEEyeJsM43-P1HfdVKpnaoH1mQc";
export const createConfig = () => {
  return {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "bearer " + userLocalService.get()?.accessToken,
  };
};

export const https = axios.create({
  baseURL: BASE_URL,
  headers: createConfig(),
});

https.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    store.dispatch(setLoadingOn());
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    store.dispatch(setLoadingOff());
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    store.dispatch(setLoadingOff());
    return Promise.reject(error);
  }
);
