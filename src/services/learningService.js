import { https } from "./configURL";

export const learningService = {
  getDanhSachKhoaHoc: () => {
    return https.get(`/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP03`);
  },
  getDanhMucKhoaHoc: () => {
    return https.get(`/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc`);
  },
  getKhoaHocTheoDanhMuc: (params) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${params}&MaNhom=GP01`
    );
  },
  getChiTietKhoaHoc: (params) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${params}`
    );
  },
  getTimKiemKhoaHoc: (params) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${params}&MaNhom=GP01`
    );
  },
  postDangkyKhoaHoc: (data) => {
    return https.post(`/api/QuanLyKhoaHoc/DangKyKhoaHoc`, data);
  },
};
