import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  danhMucKhoaHoc: [],
  tenKhoaHoc: "",
};

export const khoaHocSlicer = createSlice({
  name: "khoaHocSlicer",
  initialState,
  reducers: {
    setDanhMucKhoaHoc: (state, action) => {
      state.danhMucKhoaHoc = action.payload;
    },
    setTenKhoaHoc: (state, action) => {
      state.tenKhoaHoc = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setDanhMucKhoaHoc, setTenKhoaHoc } = khoaHocSlicer.actions;

export default khoaHocSlicer.reducer;
