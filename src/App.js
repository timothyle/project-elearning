import logo from "./logo.svg";
import "./App.css";
import Layout from "./Layout/Layout";
import HomePage from "./Page/HomePage/HomePage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Loading from "./Components/Loading/Loading";
import DanhMucKhoaHocPage from "./Page/DanhMucKhoaHocPage/DanhMucKhoaHocPage";
import ChiTietPage from "./Page/ChiTietPage/ChiTietPage";
import TimKiemPage from "./Page/TimKiemPage/TimKiemPage";
import DangNhapPage from "./Page/DangNhapPage/DangNhapPage";
import DangkyPage from "./Page/DangKyPage/DangkyPage";
import LayoutSignin from "./Layout/LayoutSignin";
import NotFoundPage from "./Page/NotFoundPage/NotFoundPage";
import ThongTinUserPage from "./Page/ThongTinUserPage/ThongTinUserPage";

function App() {
  return (
    <div className="App">
      <Loading />
      <BrowserRouter>
        <Routes>
          <Route
            path="*"
            element={
              <Layout>
                <NotFoundPage />
              </Layout>
            }
          />
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route
            path="/ThongTinTaiKhoan"
            element={
              <Layout>
                <ThongTinUserPage />
              </Layout>
            }
          />
          <Route
            path="/DanhMucKhoaHoc/:MaDanhMuc"
            element={
              <Layout>
                <DanhMucKhoaHocPage />
              </Layout>
            }
          />
          <Route
            path="/ChiTiet/:maKhoaHoc"
            element={
              <Layout>
                <ChiTietPage />
              </Layout>
            }
          />
          <Route
            path="/TimKiemKhoaHoc/:tuKhoa"
            element={
              <Layout>
                <TimKiemPage />
              </Layout>
            }
          />
          <Route
            path="/DangNhap"
            element={
              <LayoutSignin>
                <DangNhapPage />
              </LayoutSignin>
            }
          />
          <Route
            path="/DangKy"
            element={
              <LayoutSignin>
                <DangkyPage />
              </LayoutSignin>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
