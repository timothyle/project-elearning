import { Button, Form, Input, message } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { userLocalService } from "../../../services/localStorageService";
import { rules } from "../../../Utilities/validatedRules";
import { setUserInfo } from "../../../redux-toolkit/slice/userSlicer";
import { userService } from "../../../services/userService";

export default function ThongTinCaNhan({ userData }) {
  const [form] = Form.useForm();
  const [editing, setEditing] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  let dispatch = useDispatch();
  let {
    taiKhoan,
    email,
    hoTen,
    soDT,
    matKhau,
    maLoaiNguoiDung,
    accessToken,
    maNhom,
  } = userData;
  useEffect(() => {}, []);

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const toggleEdit = () => {
    setEditing(!editing);
  };

  const onFinish = (values) => {
    let data = { ...values, maLoaiNguoiDung, taiKhoan, accessToken, maNhom };
    console.log(data);
    userService
      .putCapNhatThongTin(data)
      .then((res) => {
        console.log(res);
        userLocalService.set(data);
        dispatch(setUserInfo(data));
        message.success("Cập nhật thông tin thành công");
        toggleEdit();
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  const onFinishFailed = (err) => {};

  const onCancel = () => {
    form.resetFields();
    toggleEdit();
  };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      // initialValues={userData}
      labelCol={{ span: 6 }}
      className="text-start"
    >
      <div className="flex gap-10">
        <div className="w-1/2">
          <Form.Item
            label={<h3 className="font-semibold">Tên tài khoản</h3>}
            disabled
          >
            <p>{taiKhoan}</p>
          </Form.Item>
          <Form.Item
            name="hoTen"
            label={<h3 className="font-semibold">Họ và Tên</h3>}
            initialValue={hoTen}
            rules={rules.name}
          >
            {editing ? <Input /> : <p>{hoTen}</p>}
          </Form.Item>
          <Form.Item
            name="soDT"
            label={<h3 className="font-semibold">Số điện thoại</h3>}
            initialValue={soDT}
            rules={rules.phone}
          >
            {editing ? <Input type="phone" /> : <p>{soDT}</p>}
          </Form.Item>
        </div>
        <div className="w-1/2">
          <Form.Item
            labelCol={{ span: 7 }}
            name="email"
            label={<h3 className="font-semibold">Email</h3>}
            initialValue={email}
            rules={rules.email}
          >
            {editing ? <Input type="email" /> : <p>{email}</p>}
          </Form.Item>
          <Form.Item
            labelCol={{ span: 7 }}
            name="matKhau"
            label={<h3 className="font-semibold">Mật khẩu</h3>}
            rules={rules.password}
            initialValue=""
          >
            {editing ? (
              <Input.Password
                type="password"
                eye
                visibilityToggle
                onClick={handleShowPassword}
                visible={showPassword}
              />
            ) : (
              <p>{matKhau.slice(0, 2) + "********"}</p>
            )}
          </Form.Item>
          {editing ? (
            <Form.Item
              labelCol={{ span: 7 }}
              name="xacNhanMatKhau"
              label={<h3 className="font-semibold">Nhập lại mật khẩu</h3>}
              dependencies={["matKhau"]}
              rules={rules.repassword}
            >
              <Input.Password
                type="password"
                eye
                visibilityToggle
                onClick={handleShowPassword}
                visible={showPassword}
              />
            </Form.Item>
          ) : null}
        </div>
      </div>
      {editing ? (
        <>
          <Form.Item className="text-center">
            <button
              htmlType="submit"
              className="bg-yellow-400 hover:bg-yellow-300 active:bg-yellow-500 px-3 py-1 rounded duration-300"
            >
              Cập nhật
            </button>
          </Form.Item>
          <Form.Item className="text-center">
            <Button onClick={onCancel}>Cancel</Button>
          </Form.Item>
        </>
      ) : (
        <Form.Item className="">
          <Button onClick={toggleEdit}>Thay đổi thông tin cá nhân</Button>
        </Form.Item>
      )}
    </Form>
  );
}
