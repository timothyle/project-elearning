import { message } from "antd";
import React, { useEffect } from "react";
import { userService } from "../../../services/userService";

export default function ThongTinKhoaHoc({ userData }) {
  useEffect(() => {
    userService // Lỗi 403
      .postLayDanhSachKhoaHocChuaGhiDanh({ taiKhoan: userData.taiKhoan })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
        message.error(err.response.data);
      });
  }, []);

  return <div>ThongTinKhoaHoc</div>;
}
