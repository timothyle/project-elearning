import { nanoid } from "nanoid";
import React from "react";
import VideoItem from "../../../Components/VideoItem/VideoItem";

export default function ListKhoaHocMoi({ arrKhoaHoc }) {
  const renderKhoaHocMoi = () => {
    return arrKhoaHoc?.slice(0, 8).map((item) => {
      return <VideoItem key={nanoid()} item={item} />;
    });
  };
  return (
    <>
      <h2
        id="khoahocnew"
        className="text-4xl mt-5 pb-5 font-semibold border-b border-gray-200"
      >
        Các khóa học mới nhất
      </h2>
      <div className="grid grid-cols-4 gap-5 my-5">{renderKhoaHocMoi()}</div>
    </>
  );
}
