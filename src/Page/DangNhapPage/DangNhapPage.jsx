import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import Lottie from "lottie-react";
import hello_animate from "../../assets/hello-world.json";
import { NavLink, useNavigate } from "react-router-dom";
import { userService } from "../../services/userService";
import { useDispatch } from "react-redux";
import { setUserInfo } from "../../redux-toolkit/slice/userSlicer";
import { userLocalService } from "../../services/localStorageService";

export default function DangNhapPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    userService
      .postDangNhap(values)
      .then((res) => {
        let data = { ...res.data, matKhau: values.matKhau };
        message.success("Đăng nhập thành công");
        dispatch(setUserInfo(data));
        if (values.remember == true) {
          userLocalService.set(data);
        }
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div className="container mx-auto flex items-center gap-10 pt-10 h-full">
      <div className="w-1/2 flex justify-center">
        <Lottie
          animationData={hello_animate}
          loop={false}
          rendererSettings={{
            preserveAspectRatio: "xMidYMid slice",
          }}
          style={{ maxHeight: "40vh", aspectRatio: "1/1" }}
        />
      </div>
      <div className="w-1/2">
        <Form
          className="w-full"
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <h2 className="text-3xl font-bold text-sky-500 mb-5">Đăng nhập</h2>
          <Form.Item
            label="Tài khoản"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Xin vui lòng nhập tài khoản!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Xin vui lòng nhập mật khẩu!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item name="remember" valuePropName="checked">
            <Checkbox>Nhớ tài khoản</Checkbox>
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="bg-blue-500 mx-3"
            >
              Đăng nhập
            </Button>
            <NavLink to="/DangKy">
              <Button
                type="success"
                className="bg-emerald-500 text-white font-semibold duration-150 hover:bg-emerald-400 active:bg-emerald-700 border border-emerald-400 mx-3"
              >
                Đăng ký
              </Button>
            </NavLink>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
